import { Injectable } from '@angular/core';
import {Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  private teatcherMessageSource = new Subject<string>();
  teacherMessage$ = this.teatcherMessageSource.asObservable();


  constructor() { }

  sendMessage (message : string) {
    this.teatcherMessageSource.next(message);
  }
}
