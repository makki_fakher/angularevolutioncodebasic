import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit, OnChanges{
  @Input() loggedIn!: boolean;

  private _loggedIn! : boolean;
  message!: string;

  name ='fakher';
  // get loggedIn(): boolean {
  //   return this._loggedIn;
  // }
  //
  // @Input()
  // set loggedIn(value: boolean){
  //   this._loggedIn = value;
  //   if(value === true){
  //     this.message ='Welcome back fakher'
  //   } else {
  //     this.message ='please logIn'
  //   }
  // }
  constructor() {
  }
  ngOnInit(): void {
  }

  // ngOnChange work only with child-Component
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
    const loggedInValue = changes['loggedIn'];
    if (loggedInValue.currentValue === true){
      this.message ='welcome back fakher'
    }else {
      this.message ='please log in fakher!!'
    }
  }

  grretFakher(){
    alert('hello fakher ! ')
  }

}
