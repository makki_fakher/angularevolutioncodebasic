import {Component, OnInit} from '@angular/core';
import {InteractionService} from "../services/interaction.service";

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit{
 constructor(private service: InteractionService) {
 }

  ngOnInit() {
   this.service.teacherMessage$.subscribe(
     message => {
       if (message === 'good') {
         console.log('good morning teatcher !')
       } else if (message === 'done') {
         console.log('thank you teatcher')
       }
     }
   )
  }



}
