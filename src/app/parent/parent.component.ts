import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {ChildComponent} from "../child/child.component";

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements AfterViewInit{
  userLoggIn = true;
  @ViewChild(ChildComponent) childComponentRef! :ChildComponent;

  constructor( private cd : ChangeDetectorRef) {}


  // lors du chengement du variable message avec le AfterVienInit il y un problème
  // NG0100: Expression has changed after it was checked
  // por résoudre soit on ajoute le setTimeOUt ou on peut utilisé le promise

  ngAfterViewInit(): void {
    // setTimeout(()=> {
    //   this.childComponentRef.message = 'Message from parent Component';
    //
    // },0)

    // Promise.resolve().then(()=>this.childComponentRef.message ='Message from parent Component' );

    this.childComponentRef.message ='Message from parent Component'

    this.cd.detectChanges();
  }



}
