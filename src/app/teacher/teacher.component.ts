import { Component } from '@angular/core';
import {InteractionService} from "../services/interaction.service";

@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent {

  constructor(private service: InteractionService) {
  }

  greetStudent() {
    this.service.sendMessage('good');
  }

  appStudent() {
    this.service.sendMessage('done')
  }
}
