import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-child-test',
  templateUrl: './child-test.component.html',
  styleUrls: ['./child-test.component.css']
})
export class ChildTestComponent implements OnInit{

  @Output() gretEvent  = new EventEmitter();
  name = 'codeEvolution '
  ngOnInit(): void {
  }

  callPrentGreet() {
    console.log('this is child!!');
    this.gretEvent.emit(this.name);
  }


}
