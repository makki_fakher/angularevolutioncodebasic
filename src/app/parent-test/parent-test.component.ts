import { Component } from '@angular/core';

@Component({
  selector: 'app-parent-test',
  templateUrl: './parent-test.component.html',
  styleUrls: ['./parent-test.component.css']
})
export class ParentTestComponent {

  greetParent(name : string ){
    console.log('hello' + name);
  }

}
