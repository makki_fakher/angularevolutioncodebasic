import {AfterViewInit, Component, ElementRef, ViewChild} from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  implements AfterViewInit{
  pageTitle ='Angular basic';
  imgUrl = 'https://picsum.photos/200';
  count =0;
  name!: string;
  userName!: string;
  private _customerName!: string;

  @ViewChild('nameRef') nameElementRef! : ElementRef;
  // @ViewChild est accecible on ngAfterViewInit
  ngAfterViewInit(): void {
    this.nameElementRef.nativeElement.focus();
    console.log(this.nameElementRef);

  }

  get customerName(): string {
    return this._customerName;
  }

  set customerName(value: string) {
    this._customerName = value;
    if (value === 'test') {
      alert("hello test");
    }
  }

  icrmentCount(){
    this.count+=1;

  }

  updatedValue(updateVal: any) {
    this.userName = updateVal;
    if (updateVal === 'fakher') {
      alert("welcome fakher")

    }
  }

}
