import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ParentComponent} from "./parent/parent.component";
import {ChildComponent} from "./child/child.component";
import {HomeComponent} from "./home/home.component";
import {ParentTestComponent} from "./parent-test/parent-test.component";
import {ChildTestComponent} from "./child-test/child-test.component";
import {StudentComponent} from "./student/student.component";
import {TeacherComponent} from "./teacher/teacher.component";

const routes: Routes = [
  {path:'parent', component:ParentComponent},
  {path:'child', component:ChildComponent},
  {path:'home', component:HomeComponent},
  {path:'parentTest', component:ParentTestComponent},
  {path:'childTest', component:ChildTestComponent},
  {path: 'student', component: StudentComponent},
  {path:'teatcher', component:TeacherComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
